package com.fs.authCode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: fengchangxin
 * @create: 2021-10-12 17:26
 * @description:
 */
@SpringBootApplication
public class AuthCodeApplication {
    public static void main(String[] args) {
        SpringApplication.run(AuthCodeApplication.class, args);
    }
}
