package com.fs.authDB;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: fengchangxin
 * @date: 2020/10/19:22:41
 * @description:
 **/
@SpringBootApplication
public class AuthDBApplication {
    public static void main(String[] args) {
        SpringApplication.run(AuthDBApplication.class, args);
    }
}
