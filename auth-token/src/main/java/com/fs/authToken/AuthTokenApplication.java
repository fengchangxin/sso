package com.fs.authToken;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: fengchangxin
 * @date: 2021/10/10:10:24
 * @description:
 **/
@SpringBootApplication
public class AuthTokenApplication {
    public static void main(String[] args) {
        SpringApplication.run(AuthTokenApplication.class,args);
    }
}
