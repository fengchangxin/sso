package com.fs.client1Jwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: fengchangxin
 * @date: 2020/10/19:22:41
 * @description:
 **/
@SpringBootApplication
public class Client1JwtApplication {
    public static void main(String[] args) {
        SpringApplication.run(Client1JwtApplication.class, args);
    }
}
