package com.fs.client2Jwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: fengchangxin
 * @date: 2020/10/19:22:41
 * @description:
 **/
@SpringBootApplication
public class Client2JwtApplication {
    public static void main(String[] args) {
        SpringApplication.run(Client2JwtApplication.class, args);
    }
}
