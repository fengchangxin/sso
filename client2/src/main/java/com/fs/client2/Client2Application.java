package com.fs.client2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: fengchangxin
 * @date: 2020/10/19:22:41
 * @description:
 **/
@SpringBootApplication
public class Client2Application {
    public static void main(String[] args) {
        SpringApplication.run(Client2Application.class, args);
    }
}
